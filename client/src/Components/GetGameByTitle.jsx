import React, { useState, useEffect } from 'react';
import Axios from 'axios';

function GetGameByTitle({match}) {
    const [game, setGame] = useState({});
    const [resp, setResp] = useState({});
    const fetchGameByTitle = async () => {
        await Axios.get("/gamebyid/" + match.params.id).then(res => {
            setGame(res.data)
            setResp(res.data.resp)
        })
    }
    useEffect(() => {
        window.scrollTo(0, 0)
        fetchGameByTitle()
    }, []);
    
    const HandleClick = () => {
        alert("if you dont have a torrent client exemple: uTorrent, please install it first then click the Download");
        return window.location.href = resp.magnetlink;
    }
    
    return (
        <div>
        <div className="lol"><img style={{borderRadius: "20px", width: '323px', height: '433px'}} src={!resp.thumbnail ? '/images/sadFace.png': resp.thumbnail} alt={resp.title} /></div>
                <div className="lol top">
                    <ul>
                        <h2 style={{textDecoration: 'underline', color : 'lightblue',  marginLeft: ' 100px', marginBottom: '20px'}}>Scroll to bottom to download</h2>
                        <li className="a7em" style={{marginTop: '10%'}}>Size: {resp.size} GBs</li><br/>
                        <li className="a7em">Number of Files: {resp.files}</li><br />
                        <li className="a7em">Review: {resp.review + '%'} (according to users)</li><br />
                        <li className="a7em">Genre: {resp.genre}</li><br />
                        <li className="a7em">Info hash: {game.info}</li><a className="help" href="/request"><svg className="icon-svg" style={{width: '50px', top:'-5px'}} viewBox="0 0 10 10" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink">
                                <defs>
                                    <linearGradient id="myGradient" gradientTransform="rotate(90)">
                                        <stop offset="5%" stopColor="gold" />
                                        <stop offset="95%" stopColor="red" />
                                    </linearGradient>
                                </defs>
                                <circle cx="5" cy="5" r="4" fill="url('#myGradient')" />
                            </svg>
                            <h3 style={{color: 'rgb(255, 255, 255)', marginTop: '40px'}} className="icon-svg">Click here to suggest games to add ^^</h3>
                        </a>
                    </ul>
                </div>
                <hr style={{border: '1px solid pink'}} />
                <h3 className="ml-5">Trailer for {resp.title}</h3>
                <iframe width="1010" height="616"
                    src={resp.trailerlink} frameBorder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen></iframe>
                <div>
                    <div style={{textAlign: 'center'}}>
                        <h2>Join the <a href="https://discord.gg/4cfGp5Q">discord Server</a></h2>
                    </div>
                </div>
                <button onClick={HandleClick} style={{margin: "0 40%", width: '200px' ,height: '50px'}} type="button" className="mt-5 btn btn-primary">Download now!</button>
        </div>
    )
}

export default GetGameByTitle