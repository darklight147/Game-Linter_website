import React from 'react';
import {Link} from 'react-router-dom'

function GameItem(props) {
    return (
            <div className="game-list">
            <Link to={"/games/" + props.value._id}>
                <img className='content' style={{height: '325px'}} src={props.value.thumbnail} alt={props.value.title} />
                <div className="details">Size: {props.value.size}</div>
            </Link>
            </div>
    )
}


export default GameItem