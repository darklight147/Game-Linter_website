import { useEffect } from 'react';
import Axios from 'axios';


function Logout() {
    useEffect(() => {
        Axios.get("/logout")
    }, [])


    return window.location.href = '/';
}
export default Logout