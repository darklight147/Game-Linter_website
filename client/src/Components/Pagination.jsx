import React from 'react';


function Pagination ({postsPerPage, totalPosts, paginate}){
    const pageNumer = []
    for (let index = 1; index <= Math.ceil(totalPosts / postsPerPage); index++) {
        pageNumer.push(index);
    }


    return (
        <nav>
            <ul>
                {pageNumer.map((value, index) => <li key={index} className="pagination"><a onClick={() => paginate(value)} href="#" className="page-link">{value}</a></li>)}
            </ul>
        </nav>
    )
}

export default Pagination