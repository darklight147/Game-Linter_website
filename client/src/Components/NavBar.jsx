import React from 'react';
import {Link} from 'react-router-dom'


function NavBar() {
    const styles = {
        color: 'rgb(0, 255, 255)',
        textDecoration: 'none',
        fontFamily: '-apple-system, BlinkMacSystemFont ,Roboto, Oxygen, Ubuntu,  Cantarell'
    }
    return (
            <div>
                <nav className="navbar navbar-expand-lg static-top mb-5 shadow test">
                <div className="container"> 
                    <Link to='/'><h1 className="navbar-brand color">Game-Linter</h1></Link>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                    aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"> <span
                    className="navbar-toggler-icon"></span> </button>
                <div className="collapse navbar-collapse" id="navbarResponsive">
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item active"> <Link to='/request' className="nav-link" style={{color: "aliceblue"}} ><b>Not
                                    finding what you what? Click here!</b> <span className="sr-only">(current)</span> </Link> </li>
                        <li className="nav-item"> <Link to='/help' className="nav-link color">Help!</Link> </li>
                        <li className="nav-item"> <a href='https://discord.gg/4cfGp5Q' className="nav-link color" style ={styles}>| Join discord server |</a> </li>
                        <li>
                            <a href='https://blockchain.com/btc/payment_request?address=19axz8PMesRJiBqae4JZXS56M2zi8obRFA&amount=0.00011697&message=Donate 1$' className="nav-link" style={styles} >
                            | Donate 1$ <span role='img'>❤️</span></a>
                        </li>
                    </ul>
                </div>
            </div>
            </nav>
            </div>
    )
}

export default NavBar