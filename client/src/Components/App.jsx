import React from 'react';
import NavBar from './NavBar'
import AllGames from './AllGames'
import GetGameByTitle from './GetGameByTitle'
import Request from './Request'
import Logout from './Logout'
import Dashboard from './Dashboard'
//Route Elements
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'


function App() {
return (
                <Router>
                    <NavBar />
                    <div className="container">
                        <div className="card border-0 shadow my-5 look ">
                            <div className="card-body p-5 ">
                                <noscript>You need to enable JavaScript to run this app.</noscript>
                                <Switch>
                                    <Route path='/' exact component={AllGames} />
                                    <Route path='/games/:id' component={GetGameByTitle} />
                                    <Route path='/request' exact component={Request} />
                                    <Route path='/dash' exact component={Dashboard} />
                                </Switch>
                            </div>
                        </div>
                    </div>
                </Router>
        )  
}
export default App;