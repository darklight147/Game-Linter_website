import React from 'react';
import { useEffect } from 'react';
import Axios from 'axios';
import Form from './Form';
import Login from './Login';
import { useState } from 'react';


function Dashboard() {
    const [isLogged, setIsLogged] = useState(false);

    useEffect( () => {
        Axios.get("/dashboard")
        .then(res =>{setIsLogged(res.data.isLogged)});
    }, [])

    return (
        isLogged ? <Form /> : <Login />
    )
}

export default Dashboard