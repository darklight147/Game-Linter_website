import React from 'react';
import { useState } from 'react';
import Axios from 'axios';
import qs from 'qs'



function Login() {
    const [username, setUser] = useState("");
    const [password, SetPassword] = useState("");

    const HandleSubmit = (event) => {
        event.preventDefault()
        const A = Axios.post("/login", qs.stringify({
            username,
            password
        }, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })).then((r) => {
            console.log(r);
            alert('Connected!');
            window.location.reload();
        }).catch(() => {
            alert("Wrong password/email");
        })
    }

    return (
        <div>
        <div className="container">
        <div className="row">
            <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
                <div className="card card-signin my-5">
                    <div className="card-body">
                        <h5 className="card-title text-center">Sign In</h5>
                        <form className="form-signin" onSubmit={HandleSubmit}>
                            <div className="form-label-group"> <input onChange={(event) => setUser(event.target.value)} name="username" type="email" id="inputEmail"
                                    className="form-control" placeholder="Email address" required autoFocus /> <label
                                    htmlFor="inputEmail">Email address</label> </div>
                            <div className="form-label-group"> <input onChange={(event) => SetPassword(event.target.value)} name="password" type="password" id="inputPassword"
                                    className="form-control" placeholder="Password" required /> <label
                                    htmlFor="inputPassword">Password</label> </div>
                                    <input type="submit" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js"></script>
        </div>
    )
}

export default Login