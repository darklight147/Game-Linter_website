import React, { useEffect, useState } from 'react';
import GameItem from './GameItem'
import axios from 'axios'
import Carousel from './Carousel';
import Pagination from './Pagination';

function AllGames() {
    const [games, setGames] = useState([]);
    const [pics, setPicsames] = useState([]);
    const [search, setSearch] = useState("");
    const [NewGames, setNewGames] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const [PostsPerPage, setPPP] = useState(20);

    const fetchAllGames = () => {
        axios.get("/games").then(res => {
            if(res.data.resp !== undefined) {
                setGames(res.data.resp);
                setPicsames(res.data.pics);
            }
        })
    }

    const HandleChange = (event) => {
                event.preventDefault()
                setSearch(event.target.value);
                var NewGames = games.filter(game => {
                    return game.title.toLowerCase().includes(search.toLowerCase())
                })
                setNewGames(NewGames)
    }
    
    
    useEffect(() => {
        fetchAllGames()
    }, [])

    const indexOfLastPost = currentPage * PostsPerPage;
    const indexOfFirstPost = indexOfLastPost - PostsPerPage;
    const currentPosts = games.slice(indexOfFirstPost, indexOfLastPost);


    const paginate = (num) => setCurrentPage(num);


    return (
        <div>
            <div>
            <div id="carouselExampleControls" className="carousel slide" data-ride="carousel">
            <div style={{marginBottom: "20px", borderRadius: "20px"}} className="carousel-inner">
                {
                    !pics.length ? <h1>Loading Carousel...</h1> : pics.map((pic, index) => <Carousel value={pic} key={index} id={index} />)
                }
                {
                    pics.length ? <div><a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev"> <span className="carousel-control-prev-icon" aria-hidden="true"></span> <span className="sr-only">Previous</span> </a> <a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next"> <span className="carousel-control-next-icon" aria-hidden="true"></span> <span className="sr-only">Next</span> </a></div>: ""
                }
            </div>
                <div>
                    {
                        pics.length ? <input value={search} type="text" className="form-control mb-5" onChange={HandleChange} placeholder="Search..." /> : ""
                    }
                </div>
            </div>
                {
                    !search ?  (!currentPosts.length ? <h1>Fetching games from database...</h1> : currentPosts.map((game, index) => <GameItem key={index} value={game} />)) : NewGames.map((game, index) => <GameItem key={index} value={game} />)
                }
            <Pagination postsPerPage={PostsPerPage} totalPosts={games.length} paginate={paginate} />
            </div>
        </div>
    )
}

export default AllGames