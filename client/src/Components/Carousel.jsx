import React, { useEffect} from 'react';
import Axios from 'axios';
import { useState } from 'react';

import {Link} from 'react-router-dom'



function Carousel(props) {
    
        return (
                <div className={"carousel-item" + (!props.id ? " active" : "")}> <Link to={"/games/" + props.value._id}> <img className="d-block w-100 h-40"
                src={props.value.backgroundimg} alt={props.value.title} />
            <div className="carousel-caption d-none d-md-block">
                <h5>{props.value.title}</h5>
            </div>
            </Link>
        </div>
        )
}

export default Carousel;