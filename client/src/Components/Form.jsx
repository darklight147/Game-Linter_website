import React from 'react';
import { useState } from 'react';
import Axios from 'axios';
import qs from 'qs'

function Form() {
    const [title, setTitle] = useState("");
    const [size, setsize] = useState(null);
    const [Genre, setGenre] = useState("");
    const [magnetLink, setmagnetLink] = useState("");
    const [review, setreview] = useState(null);
    const [Thumbnail, setThumbnail] = useState("");
    const [background, setbackground] = useState("");
    const [trailer, settrailer] = useState("");
    const [NumberofFiles, setNumberofFiles] = useState(null);

    const HandleSubmit = (event) => {
        event.preventDefault()
        Axios.post("/secret", qs.stringify({
            title, 
            size,
            magnetLink,
            Genre,
            Thumbnail,
            review,
            background,
            trailer,
            NumberofFiles
        })).then(() => {
            alert("Game added, successfuly");
            window.location.href = '/';
        })
    }
    return (
        <div>
            <h1 className="font-weight-light ">Add new games to ur website</h1>
            <form onSubmit={HandleSubmit}>
                <div className="form-row">
                    <div className="form-group col-md-6"> <label for="inputEmail4">Title</label> 
                    
                <input value={title} onChange={(event) => setTitle(event.target.value)} name="title" type="text" className="form-control" id="inputEmail4" placeholder="Title" required /> </div>
                <div className="form-group col-md-6"> <label for="inputPassword4">Size in GB</label> 
                    
                <input value={size}  onChange={(event) => setsize(event.target.value)} name="size" type="text" className="form-control" id="inputPassword4" placeholder="size" required /> </div>
                </div>
                <div className="form-group"> <label for="inputAddress">Genre</label> 
                <input value={Genre} onChange={(event) => setGenre(event.target.value)} name="genre" type="text" className="form-control" id="inputAddress" placeholder="game genre" required /> </div>
                <div className="form-group"> <label for="inputAddress2">Number of files</label>
                <input value={NumberofFiles} onChange={(event) => setNumberofFiles(event.target.value)} name="files" type="number" className="form-control" id="inputAddress2" placeholder="in numbers..." required /> </div>
                <div className="form-group"> <label for="inputAddress2"><b>Magnet</b> link</label> 
                <input value={magnetLink} onChange={(event) => setmagnetLink(event.target.value)} name="magnetlink" type="text" className="form-control" id="inputAddress2" placeholder="" required autocomplete="off" /> </div>
                <div className="form-group"> <label for="inputAddress2">Review</label>
                <input value={review} onChange={(event) => setreview(event.target.value)} name="review" max="100" min="0" type="number" className="form-control" id="inputAddress2" required placeholder="in percent..." /> </div>
                <div className="form-group"> <label for="inputAddress2">Thumbnail link</label>
                <input value={Thumbnail} onChange={(event) => setThumbnail(event.target.value)} name="thumbnail" type="text" className="form-control" id="inputAddress2" required placeholder="link" autocomplete="off" /> </div>
                <div className="form-group"> <label for="inputAddress2">background link</label>
                <input value={background} onChange={(event) => setbackground(event.target.value)} name="backgroundimg" type="text" className="form-control" id="inputAddress2" required placeholder="link" autocomplete="off" /> </div>
                <div className="form-group"> <label for="inputAddress2">Trailer Link</label>
                <input value={trailer} onChange={(event) => settrailer(event.target.value)} name="trailerlink" type="text" className="form-control" id="inputAddress2" required placeholder="link" autocomplete="off" /> </div><button type="submit" className="btn btn-primary">Done!</button> <button onClick={(event) => {event.preventDefault(); Axios.get("/logout").then(() => window.location.reload()); }} className="btn btn-warning" >Sign out!</button>
            </form>
        </div>
    )
}

export default Form