import React from 'react';
import { useState } from 'react';
import axios from 'axios'
import qs from 'qs'

function Request() {
    
    const [request, setRequest ] = useState("");
    const onChange = (event) => {
        setRequest(event.target.value)
    } 
    const onSubmit = (event) => {
        event.preventDefault()
        axios.post("/request", qs.stringify({lmao: request})).then(() => alert("Thank you!"));
    }

    return (
        <form onSubmit={onSubmit}>
            <div className="form-group row"> <label htmlFor="staticEmail" className="col-sm-2 col-form-label">Platform</label>
                <div className="col-sm-10"> <input style={{color: "white"}} type="text" readOnly className="form-control-plaintext"
                        id="staticEmail" value="PC" /> </div>
            </div>
            <div className="form-group row"> <label htmlFor="inputPassword" className="col-sm-2 col-form-label">Request</label>
                <div className="col-sm-10"> <input onChange={onChange} type="text" name="title" className="form-control" id="inputPassword"
                        placeholder="the game u'd like" required value={request}/> </div>
            </div><input className="btn btn-light" type="submit" value="Request" />
        </form>
    )


}

export default Request