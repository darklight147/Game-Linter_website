const mongoose = require("mongoose"),
    ejs = require("ejs"),
    bodyParser = require("body-parser"),
    lodash = require("lodash"),
    express = require("express"),
    helmet = require("helmet"),
    app = express(),
    passport = require("passport"),
    LocalStrategy = require("passport-local").Strategy,
    bcrypt = require("bcrypt"),
    session = require("express-session"),
    morgan = require('morgan'),
    requestIp = require('request-ip'),
    dotenv = require('dotenv'),
    path = require('path'),
    moment = require('moment');


dotenv.config({
    path: '.env'
});
mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: !0,
    useUnifiedTopology: !0
}).then(() => console.log("connected to db")).catch(e => console.log(e))

const gameSchema = new mongoose.Schema({
        title: String,
        magnetlink: String,
        size: Number,
        files: Number,
        review: Number,
        genre: String,
        thumbnail: String,
        backgroundimg: String,
        trailerlink: String
    }),
    requestSchema = new mongoose.Schema({
        title: String,
        date: {
            type: String
        }
    }, {
        timestamps: true
    }),
    userSchema = new mongoose.Schema({
        username: String,
        password: String
    }),
    IPsChema = new mongoose.Schema({
        adresse: String
    }),
    IP = mongoose.model('Ip', IPsChema),
    User = mongoose.model("User", userSchema),
    Request = mongoose.model("Request", requestSchema),
    Game = mongoose.model("Game", gameSchema),


    
    port = process.env.PORT || 5e3;






app.use(morgan('dev')), app.use(requestIp.mw()), app.use(bodyParser.urlencoded({
        extended: !0
    })), app.use(session({
        secret: "cats",
        resave: true,
        saveUninitialized: false
    })), app.use(passport.initialize()), app.use(passport.session()),
    app.use(helmet()), passport.serializeUser((e, r) => {
        r(null, e.id)
    }), passport.deserializeUser((e, r) => {
        User.findById(e, (e, s) => {
            r(e, s)
        })
    }),

    passport.use(new LocalStrategy((e, r, s) => {
        User.findOne({
            username: e
        }, (e, t) => e ? s(e) : t ? void bcrypt.compare(r, t.password, (e, r) => {
            return r ? s(null, t) : s(null, !1, {
                message: "Incorrect password."
            })
        }) : s(null, !1, {
            message: "Incorrect username."
        }))
    }))



app.get("/games", (e, r) => {
    const ip = e.clientIp;
    IP.findOne({
        adresse: ip
    }, (err, res) => {
        if (err) {
            console.log(err);
        } else {
            if (!res) {
                new IP({
                    adresse: ip
                }).save()
            }
        }
    })
    if (void 0 !== e.query.q) {
        var s = lodash.toUpper(e.query.q);
        Game.find({
            title: {
                $regex: s
            }
        }, (e, s) => {
            if (e) throw e;
            0 !== s.length ? r.status(200).json({
                resp: s,
                currentpage: 1,
                a: 1,
                b: 0
            }) : r.status(200).json({
                resp: [],
                currentpage: 1,
                a: 1,
                b: 0
            })
        })
    } else {
        const e = 20,
            s = 1;
        Game.find({}, (e, s) => {
            e ? console.log(e) : s ? Game.find({}, (e, t) => {
                r.status(200).json({
                    resp: s,
                    currentpage: 1,
                    a: 1,
                    b: 1,
                    pics: t
                })
            }).sort({
                _id: -1
            }).limit(5) : console.log(s)
        }).sort({
            _id: -1
        })
    }
})
//ends here

app.get("/dashboard", (e, r) => {
        e.isAuthenticated() ? r.json({
            isLogged: true
        }) : r.json({
            isLogged: false
        })
    }), app.get("/logout", (e, r) => {
        e.logout(), e.session.destroy(err => {
            err ? console.log('Error: failed to destory session', err) : null
            e.user = null;
            r.redirect('/');
        });
    }), app.get("/gamebyid/:id", (e, r) => {
        const s = e.params.id;
        Game.findOne({
            _id: s
        }, (e, s) => {
            e ? console.log(e) : s ? r.json({
                resp: s,
                info: s.magnetlink.slice(20, s.magnetlink.indexOf("&"))
            }) : r.json({
                msg: 404
            })
        })
    }), app.post("/secret", (e, r) => {
        if (e.isAuthenticated()) {
            const {
                title: s,
                size: t,
                NumberofFiles: o,
                magnetLink: n,
                review: a,
                Genre: i,
                Thumbnail: p,
                background: l,
                trailer: g
            } = e.body, m = "https://www.youtube.com/embed/" + g.slice(32, g.lenght);
            new Game({
                title: lodash.toUpper(s).trim(),
                size: lodash.toLower(t),
                files: lodash.toLower(o),
                magnetlink: n.trim(),
                review: a,
                genre: i,
                thumbnail: p,
                backgroundimg: l,
                trailerlink: m
            }).save(e => {
                e ? console.log(e) : (console.log("Game added successfully"), r.redirect("/"))
            })
        }
    }), app.post("/request", (e, r) => {
        var s = e.body.lmao;
        new Request({
            title: s,
            date: moment().format('MMMM Do YYYY, h+1:mm:ss a').toString()
        }).save(e => {
            e ? console.log(e) : r.redirect("/")
        })

    }),

    app.post("/login", passport.authenticate("local", {
        successRedirect: '/',
        failureFlash: false,
        successMessage: "success"
    }));


if (process.env.NODE_ENV === "production") {
    app.use(express.static(path.join(__dirname, 'client/build')));
    app.get('/*', (req, res) => {
        res.sendFile(path.join(__dirname, 'client', 'build', 'index.html'));
    })
}

app.listen(port, console.log("app runnning on http://localhost:" + port + "/games"));